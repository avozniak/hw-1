# Homework #1 for Pivorak Ruby Summer Course!

## Your task is to:

1) Write good bill composition algorithm!  
2) Pass style guide check (and maybe check/fix style for your test project :)  
3) Pass bill composition tests  
4*) Pass additional bill composition tests  

## Attention!

1) All changes to files in `spec` folder are strictly prohibited.  
2) To check additional task completion you should make `ADDITIONAL_TASK = true`.  
