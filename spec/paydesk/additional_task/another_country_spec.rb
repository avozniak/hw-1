# frozen_string_literal: true

require './lib/paydesk'

describe Paydesk do
  let!(:bills_hash) { { '10000': 1, '5000': 2, '1000': 3 } }

  it 'returns expected hash' do
    skip unless Paydesk::ADDITIONAL_TASK

    expect(described_class.new(bills_hash, 8000).call).to eq('5000': 1, '1000': 3)
  end
end
