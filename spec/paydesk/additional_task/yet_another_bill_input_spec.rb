# frozen_string_literal: true

require './lib/paydesk'

describe Paydesk do
  let!(:bills_hash) { "100:2\t20:3\t5:1\t1:15" }

  it 'returns expected hash' do
    skip unless Paydesk::ADDITIONAL_TASK

    expect(described_class.new(bills_hash, 19).call).to eq("5:1\t1:14")
  end
end
