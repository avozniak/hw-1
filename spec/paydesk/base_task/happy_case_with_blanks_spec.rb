# frozen_string_literal: true

require './lib/paydesk'

describe Paydesk do
  let!(:bills_hash) do
    {
      '500': 0,
      '100': 0,
      '50': 4,
      '20': 2,
      '10': 6,
      '5': 0,
      '2': 0,
      '1': 2
    }
  end

  it 'returns expected hash' do
    expect(described_class.new(bills_hash, 302).call).to eq('50': 4, '20': 2, '10': 6, '1': 2)
  end
end
