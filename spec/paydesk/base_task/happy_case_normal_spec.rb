# frozen_string_literal: true

require './lib/paydesk'

describe Paydesk do
  let!(:bills_hash) { { '100': 2, '20': 3, '10': 5, '5': 1, '1': 10 } }

  it 'returns expected hash' do
    expect(described_class.new(bills_hash, 76).call).to eq('20': 3, '10': 1, '5': 1, '1': 1)
  end
end
